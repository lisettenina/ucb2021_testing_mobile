Feature: Task
  @Appium
  Scenario: Ccomo usuario en la app
            Quiero crear tareas con título y descripción
            Para realizar seguimiento a mis actividades

    Given la app WhenDo esta abierta
    And yo hago clic en el boton agregarTarea
    When yo creo una tarea
        |titulo       | Clean                   |
        |descripcion  | Esto es una descripcion |
    And realizo clic en boton guardar
    Then la tarea con el nombre "Clean" deberia ser creada