package runner;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;
import session.Session;
import java.net.MalformedURLException;

@RunWith(Cucumber.class)
public class Runner {
    @Before
    public void hookBefore(){
        System.out.println("esto se ejecuta al inicio de un scenario");
    }
    @After
    public void hookAfter() throws MalformedURLException{
        System.out.println("esto se ejecuta al final de un scenario");
        Session.getInstance().closeSession();
    }
}