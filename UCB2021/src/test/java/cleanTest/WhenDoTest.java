package cleanTest;

import activities.whenDo.CreateTaskScreen;
import activities.whenDo.DeleteTask;
import activities.whenDo.MainScreen;
import activities.whenDo.UpdateTaskScreen;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import session.Session;

import java.net.MalformedURLException;

public class WhenDoTest {
    private MainScreen mainScreen= new MainScreen();
    private CreateTaskScreen createTaskScreen= new CreateTaskScreen();
    private DeleteTask deleteTask =new DeleteTask();
    private UpdateTaskScreen updateTaskScreen =new UpdateTaskScreen();

    @Test
    public void verifyCreateTask() throws MalformedURLException {
        String title="CLEAN";
        mainScreen.addTaskButton.click();
        createTaskScreen.titleTextBox.setValue(title);
        createTaskScreen.descriptionTextBox.setValue("this is a description");
        createTaskScreen.saveButton.click();

        Assertions.assertEquals(title,mainScreen.nameTaskLabel.getText(),"ERROR! task was not created");

    }
    @Test
    public void verifyUpdateTask() throws MalformedURLException {
        this.verifyCreateTask();

        String titleUpdate = "task";
        String descriptionUpdate = "note update";
        mainScreen.nameTaskLabel.click();

        createTaskScreen.titleTextBox.setValue(titleUpdate);
        createTaskScreen.descriptionTextBox.setValue(descriptionUpdate);
        createTaskScreen.saveButton.click();

        Assertions.assertEquals(titleUpdate, mainScreen.nameTaskLabel.getText(), "ERROR! title was not update");
        Assertions.assertEquals(descriptionUpdate, updateTaskScreen.descriptionTaskLabel.getText(), "ERROR! description was not update");
    }

    @Test
    public void verifyDeleteTask() throws MalformedURLException {
        this.verifyCreateTask();

        mainScreen.nameTaskLabel.click();
        deleteTask.deleteButton.click();
        deleteTask.confirmationButton.click();

        Assertions.assertEquals("No se agregaron tareas", mainScreen.noNotes.getText(), "ERROR! task was not delete");
    }

    @AfterEach
    public void close() throws MalformedURLException {
        Session.getInstance().closeSession();
    }

}
