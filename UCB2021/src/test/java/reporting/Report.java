package reporting;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Report {
    public static void main (String []args)throws InterruptedException {

        // donde va generar file.json
        Thread.sleep(20000);

        String ruta=new File(".").getAbsolutePath().replace(".","")+"build\\reports\\cucumber\\";
        File reportFolder= new File(ruta+"UCBMovileReport");

        List<String> jsonList= new ArrayList<>();
        jsonList.add(ruta+"report.json");


        // configuraciones
        Configuration configuration = new Configuration(reportFolder,"UCB Modulo 3");
        configuration.setBuildNumber("00001");
        configuration.addClassifications("Owner","UCB");
        configuration.addClassifications("Branch","Master");
        configuration.addClassifications("OS","Windows10");
        configuration.addClassifications("Suite","Appium");


        // creacion reporte

        ReportBuilder reportBuilder=  new ReportBuilder(jsonList,configuration);
        reportBuilder.generateReports();

    }
}
