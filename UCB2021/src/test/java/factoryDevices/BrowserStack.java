package factoryDevices;

        import io.appium.java_client.AppiumDriver;
        import io.appium.java_client.android.AndroidDriver;
        import io.appium.java_client.android.AndroidElement;
        import org.openqa.selenium.remote.DesiredCapabilities;

        import java.net.MalformedURLException;
        import java.net.URL;

public class BrowserStack implements IDevice{
    @Override
    public AppiumDriver create() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();

        // Set your access credentials
        caps.setCapability("browserstack.user", "lisetten_uQqSBe");
        caps.setCapability("browserstack.key", "KudYozBKUENQo82SfAvY");

        // Set URL of the application under test
        caps.setCapability("app", "bs://ba3e88623341ca99f6d571168af39469805df55c");

        // Specify device and os_version for testing
        caps.setCapability("device", "Google Pixel 3");
        caps.setCapability("os_version", "9.0");

        // Set other BrowserStack capabilities
        caps.setCapability("project", "UCB Diplomado Testing");
        caps.setCapability("build", "Modulo 3");
        caps.setCapability("name", "TEST3");


        // Initialise the remote Webdriver using BrowserStack remote URL
        // and desired capabilities defined above
        AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                new URL("http://hub.browserstack.com/wd/hub"), caps);

        return driver;

    }
}