package basicTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BasicJunit {

    @BeforeEach
    public void setup(){
        System.out.println("SETUP");
    }

    //cleanup - teardown
    @AfterEach
    public void cleanup(){
        System.out.println("CLEANUP");
    }

    @Test
    public void thisIsTest(){
        System.out.println("TEST");
    }

    @Test
    public void thisIsATest2(){
        System.out.println("TEST2");
    }


}
